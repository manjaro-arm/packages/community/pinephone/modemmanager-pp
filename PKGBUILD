# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>

pkgbase=modemmanager
pkgname=(modemmanager-pp libmm-pp-glib)
pkgver=1.20.4
pkgrel=1
pkgdesc="Mobile broadband modem management service (PinePhone)"
arch=(x86_64 armv7h aarch64)
url="https://www.freedesktop.org/wiki/Software/ModemManager/"
license=(GPL2 LGPL2.1)
depends=(systemd libgudev polkit ppp 'libqmi>=1.30.2-1' libmbim
         mobile-broadband-provider-info)
makedepends=(gtk-doc gobject-introspection vala autoconf-archive git)
_commit=6926459500fd927e7cceb589a9e4113d4edb04e6  # tags/1.20.4^0
source=(git+https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git#commit=$_commit
        $pkgname.rules)
sha256sums=('SKIP' 'SKIP')

pkgver() {
  cd ModemManager
  git describe --tags | sed 's/-rc/rc/;s/-/+/g'
}

prepare() {
  cd ModemManager

  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done

  # https://bugs.archlinux.org/task/74329
  chmod -x plugins/fibocom/77-mm-fibocom-port-types.rules \
           plugins/foxconn/mm-foxconn-t77w968-carrier-mapping.conf

  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd ModemManager
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --sbindir=/usr/bin \
    --with-dbus-sys-dir=/usr/share/dbus-1/system.d \
    --with-udev-base-dir=/usr/lib/udev \
    --with-polkit=permissive \
    --with-systemd-journal \
    --with-at-command-via-dbus \
    --with-systemd-suspend-resume=yes \
    --enable-compile-warnings=yes \
    --enable-gtk-doc \
    --disable-static

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package_modemmanager-pp() {
  depends+=(libmm-pp-glib libmm-glib.so libg{lib,object,io,module}-2.0.so libsystemd.so libgudev-1.0.so
            libqmi-glib.so libmbim-glib.so)
  optdepends=('usb_modeswitch: install if your modem shows up as a storage drive')
  options=(!emptydirs)
  provides=(modemmanager modemmanager-settings-pp)
  conflicts=(modemmanager)
  replaces=(modemmanager-settings-pp)

  cd ModemManager
  make DESTDIR="$pkgdir" install
  make DESTDIR="$pkgdir" -C libmm-glib uninstall
  make DESTDIR="$pkgdir" -C vapi uninstall

  install -d -o root -g 102 -m 750 "${pkgdir}/usr/share/polkit-1/rules.d"
  install -m644 -D "$srcdir/$pkgname.rules" \
        "$pkgdir/usr/share/polkit-1/rules.d/01-org.freedesktop.ModemManager.rules"
        
  # Skip the traditional suspend/resume cycle and synchronizes ModemManager with the 
  # internal state of always-powered modems with a suspended host
  sed -i -e 's|bin/ModemManager|bin/ModemManager --test-quick-suspend-resume|g' \
        "$pkgdir/usr/lib/systemd/system/ModemManager.service" 
  
  # Some stuff to move is left over
  mv "$pkgdir/usr/include" ..
  mv "$pkgdir/usr/lib/pkgconfig" ..
}

package_libmm-pp-glib() {
  pkgdesc="ModemManager library (PinePhone)"
  depends=(libg{lib,object,io}-2.0.so)
  provides=(libmm-glib.so libmm-glib)
  conflicts=(libmm-glib)

  install -d "$pkgdir/usr/lib"
  mv include "$pkgdir/usr"
  mv pkgconfig "$pkgdir/usr/lib"

  cd ModemManager
  make DESTDIR="$pkgdir" -C libmm-glib install
  make DESTDIR="$pkgdir" -C vapi install
}
